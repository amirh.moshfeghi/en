const app = require("./server")
const supertest = require('supertest')
describe('POST /', function () {
  it('category is valid', function (done) {
      supertest(app)
          .get("/api/v1/category/")
          .set('Accept', 'application/json')
          .expect(response => {
            expect(response.status).toBe(200)
            expect(response.body).toEqual({data: [], code: 0, status: 200, description: ""})
            done()
  });
  it('archive is valid', function (done) {
    supertest(app)
        .get("/api/v1/ads/archive")
        .set('Accept', 'application/json')
        .expect(response => {
          expect(response.status).toBe(200)
          expect(response.body).toEqual({data: [], code: 0, status: 200, description: ""})
          done()
});
  it('bargain is valid', function (done) {
    supertest(app)
        .get("/api/v1/bargain/")
        .set('Accept', 'application/json')
        .expect(response => {
          expect(response.status).toBe(200)
          expect(response.body).toEqual({"data":{"id":179,"username":"moshfeghi","system_code":"680c599a-c964-4820-87b5-ae7e2611885d","type":1,"price":0.0,"created_date":"2020-05-01T15:10:50.212994Z","expiration":"2020-05-03T15:10:50.211706Z","result":0,"archiveOwner":0,"archiveDst":0,"pending":true,"step":3,"user":23,"ad":17},"code":0,"status":201,"description":""})
          done()
});

  it('promotion is valid', function (done) {
    supertest(app)
        .get("/api/v1/promotion/")
        .set('Accept', 'application/json')
        .expect(response => {
          expect(response.status).toBe(200)
          expect(response.body).toEqual({"data":"","code":0,"status":302,"description":"the contact exist on your contact"})
          done()
});

  describe('GET /', function () {
    it('area is valid', function (done) {
      supertest(app)
          .get("/api/v1/area")
          .set('Accept', 'application/json')
          .expect(response => {
            expect(response.status).toBe(200)
            expect(response.body).toEqual({"data":"","code":0,"status":200,"description":""})
            done()
  });
 
    it('currency is valid', function (done) {
        supertest(app)
            .get("/api/v1/currency")
            .set('Accept', 'application/json')
            .expect(response => {
              expect(response.status).toBe(200)
              expect(response.body).toEqual({"data":"","code":0,"status":200,"description":""})
              done()
    });
    it('ads is valid', function (done) {
      supertest(app)
          .get("/api/v1/ads")
          .set('Accept', 'application/json')
          .expect(response => {
            expect(response.status).toBe(200)
            expect(response.body).toEqual({"data":"","code":0,"status":200,"description":""})
            done()
  });
 
   
});
});
});
});
});
});
});
